$LOAD_PATH.unshift File.expand_path("../lib", __dir__)
require "executioner"

require "minitest/autorun"
require 'mocha/minitest'

require 'minitest/reporters'
MiniTest::Reporters.use!