require "test_helper"

module Executioner
  class HangmanTest < Minitest::Test
    def setup
      @version = "2.54.0"
      @logger = ->(_message) { 42 }

      @hangman = Hangman.new(->() { @version }, @logger)
    end

    def test_deprecate_logs_message
      @logger = ->(message) { assert_equal "logged!", message }
      @hangman.stubs(:maybe_die).returns(false)
      @hangman.deprecate("2.54.0", "logged!")
    end

    def test_does_not_die_if_rails_production
      @hangman.stubs(:rails_production?).returns(true)
      @hangman.expects(:die!).never
      @hangman.deprecate("42.5.2", "Deprecated!")
    end

    def test_die_is_called_if_should_die_true
      @hangman.expects(:die!).returns(false).once
      @hangman.stubs(:should_die?).returns(true)
      @hangman.stubs(:rails_production?).returns(false)
      @hangman.deprecate('42.2.2', "message")
    end

    def test_should_die_behaves_correctly
      cases = [
        ["5", {
          "5" => true,
          "4" => false,
          "6" => true,
          "4.99" => false,
          "4.5.4" => false
        }],
        ["5.1", {
          "5" => false,
          "5.1" => true,
          "4" => false,
          "6" => true,
          "5.0.8" => false,
          "4.5.4" => false
        }],
        ["5.1.9", {
          "5" => false,
          "5.1" => false,
          "5.1.9" => true,
          "4" => false,
          "6" => true,
          "5.1.8" => false,
          "4.5.4" => false
        }]
      ]

      cases.each do |(version, tests)|
        @hangman.stubs(:current_version).returns(Gem::Version.new(version))

        tests.each do |v, expected|
          assert_equal expected, @hangman.should_die?(v)
        end
      end
    end
  end
end