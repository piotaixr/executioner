module Executioner
  class Hangman
    attr_reader :version_provider, :logger, :current_version, :puts_on_double_fault

    def initialize(version_provider, logger, puts_on_double_fault: true)
      @version_provider = version_provider
      @logger = logger
      @current_version = Gem::Version.new(version_provider.call)
      @puts_on_double_fault = puts_on_double_fault
    end

    def deprecate(version, message)
      logger.(message)
      maybe_die(version)
    rescue StandardError => e
      puts e.message if puts_on_double_fault
    end

    def should_die?(version)
      Gem::Version.new(version) >= current_version
    end

    private

      def rails_production?
        @rails_production ||= defined?(Rails) && Rails.env.production?
      end

      def maybe_die(version)
        return if rails_production?

        if should_die?(version)
          die!
        end
      end

      def die!
        abort "The grace period is expired, fix this and retry!"
      end

  end
end